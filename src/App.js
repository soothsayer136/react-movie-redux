
import './App.css';
import MoviesSearch from './components/MoviesSearch';
import MovieList from  './components/MovieList';

function App() {
  return (
    <div className="App">
     <MoviesSearch/>
     <MovieList/>
    </div>
  );
}

export default App;
