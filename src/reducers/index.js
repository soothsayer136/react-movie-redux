import { combineReducers } from "redux";
import { SET_MOVIES } from "../actions";

function movies (state=[], action) {
    switch(action.type) {
        case SET_MOVIES:
            return action.info;
            default:
                return state;

              
    }
}

const rootReducers = combineReducers({movies});
export default rootReducers;