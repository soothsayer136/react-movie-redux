export const SET_MOVIES = 'SET_MOVIES';

export function setMovies (info) {
    return {
        type: SET_MOVIES,
        info
    }
};