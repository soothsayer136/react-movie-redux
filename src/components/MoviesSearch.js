import React, { Component } from 'react';
import axios from 'axios';
import { connect }  from 'react-redux';
import { setMovies } from '../actions';


class MoviesSearch extends Component {

    constructor() {
        super();
        this.state ={
            movieName:''
        }
    }

    search() {
        let{movieName} = this.state;
        const url =`http://www.omdbapi.com/?t=${movieName}&apikey=27dfc216`;

        axios.get(url)
        .then((response) => {
            // console.log(response)
            this.props.setMovies(response.data)
        })
    }


    

    render() {
        return (
            <div className="form-group" style={{display:"flex", width:500, height:50, marginLeft:500, marginTop:150}}>
              
                <input type="text" className="form-control" id name="movieName" placeholder="Enter Movie Name"
                    onChange={e => this.setState({movieName: e.target.value})}
                />
                <button onClick={() => this.search()} className="btn btn-danger">Search</button>
                
            </div>
        );
    }
}

export default connect(null, {setMovies})(MoviesSearch);