import React, { Component } from 'react';
import { connect } from 'react-redux';

class MovieList extends Component {
    render() {
        // console.log('this.props', this.props.movies)
        return (
            <div style={{ marginLeft:200,marginTop:50,display:"flex", alignItems:"center", flexDirection:"column"}}>
            <table style = {{width:700, border: '1px solid black'}}>
                <thead style={{border: '1px solid black'}}>
                    	<th>Name</th>
                    <th>Director</th>
                    <th>BoxOffice</th>
                    <th>Type</th>
                    <th>Poster</th>
                </thead>
                <tbody>
                <tr>
                    <td>{this.props.movies.Title}</td>
                    <td>{this.props.movies.Director}</td>
                    <td>{this.props.movies.BoxOffice}</td>
                    <td>{this.props.movies.Type}</td>
                    <td><img src={this.props.movies.Poster} width="300" height="200"/></td>
                </tr>
                   
                </tbody>
            </table>
        </div>
            
        );
    }
}

function mapStateToProps(state) {
    // console.log(state)
    return state
}

export default connect(mapStateToProps, null)(MovieList);